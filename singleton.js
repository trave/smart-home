module.exports = function(key, init) {
    const KEY = Symbol.for(key);

    if (!Object.getOwnPropertySymbols(global).includes(KEY)) {
        global[KEY] = init();
    }

    return global[KEY];
};
