const RaspberryButton = require('./../lib/raspberry-button');
const hapUtils = require('./../lib/utils');
const Types = require('./../lib/types');

module.exports = (accessories) => {
	const pair = ({
		button,
		mac,
		maxLight,
		defaultLight = 0.5,
		increaseStep = 0.1,
		increaseInterval = 300,
	}) => {
		const power = hapUtils.findCharacteristic(accessories, mac, 'power', Types.POWER_STATE_CTYPE);
		const brightness = hapUtils.findCharacteristic(accessories, mac, 'power', Types.BRIGHTNESS_CTYPE);

		if (brightness.value === 0) {
			brightness.setValue(maxLight * defaultLight);
		}

		if (!power || !brightness) {
			console.error('Can\'t find required accessory with MAC', mac);
			return;
		}

		if (brightness.value === 0) {
			brightness.setValue(maxLight * defaultLight);
		}

		const setLight = (value) => {
			value = Math.round(value);
			return new Promise((resolve, reject) => {
				brightness.setValue(value, (err) => {
					if (err) {
						reject(err);
					} else {
						resolve();
					}
				});
			});
		};
		const toggleState = (defaultLevel) => {
			const changePower = new Promise((resolve, reject) => {
				const isTurnedOn = !power.value;
				power.setValue(isTurnedOn, (err) => {
					if (err) {
						reject(err);
					} else {
						resolve(isTurnedOn);
					}
				});
			});

			return changePower
				.then((isTurnedOn) => {
					if (isTurnedOn) {
						return setLight(maxLight * defaultLevel)
							.then(() => isTurnedOn);
					}

					return isTurnedOn;
				});
		};

		let increaseLightTimeout = NaN;
		let prevLightLevel = NaN;

		const increase = () => {
			prevLightLevel += increaseStep;
			if (prevLightLevel > 1) {
				return;
			}

			setLight(maxLight * prevLightLevel)
				.then(() => {
					increaseLightTimeout = setTimeout(increase, increaseInterval);
				});
		};

		let isButtonPressed = false;
		button.on(button.EVENT_DOWN, () => {
			isButtonPressed = true;
			toggleState(defaultLight)
				.then((isTurnedOn) => {
					if (isTurnedOn) {
						prevLightLevel = defaultLight;
						if (isButtonPressed) {
							increaseLightTimeout = setTimeout(increase, increaseInterval);
						}
					}
				}, (err) => console.error(err));
		});

		button.on(button.EVENT_UP, () => {
			isButtonPressed = false;
			clearTimeout(increaseLightTimeout);
		});
	};

	pair({
		button: new RaspberryButton(22),
		mac: '1A:2B:3C:4D:5E:ED',
		maxLight: 100
	});
	pair({
		button: new RaspberryButton(17),
		mac: '1A:2B:3C:4D:5E:EC',
		maxLight: 100
	});
	pair({
		button: new RaspberryButton(27),
		mac: '1A:2B:3C:4D:5E:1D',
		maxLight: 100,
		defaultLight: 0.25,
		increaseStep: 0.03
	});
};
