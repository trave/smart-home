const hapUtils = require('./../lib/utils');
const Types = require('./../lib/types');

module.exports = (accessories) => {
	const humidityCharacteristic = hapUtils.findCharacteristic(
		accessories,
		'1A:2B:3C:4D:11:EC',
		'humidity',
		Types.CURRENT_RELATIVE_HUMIDITY_CTYPE
	);
	const fanPowerCharacteristic = hapUtils.findCharacteristic(
		accessories,
		'1A:2B:3C:4D:01:EC',
		'power',
		Types.POWER_STATE_CTYPE
	);

	const MIN_HUMIDITY = 7;
	const MAX_HUMIDITY = 8;
	const MIN_COOLER_WORK_TIME = 10 * 1000;

	let delayed = NaN;
	let startTime = 0;
	let coolerVal = fanPowerCharacteristic.value;

	humidityCharacteristic.on('change', (eventData) => {
		var humidity = eventData.newValue;

		if (coolerVal === true && humidity < MIN_HUMIDITY) {
			coolerVal = false;
			var minTime = MIN_COOLER_WORK_TIME - Date.now() - startTime;
			if (minTime > 0) {
				delayed = setTimeout(() => {
					fanPowerCharacteristic.updateValue(false);
				}, minTime);
			} else {
				fanPowerCharacteristic.updateValue(false);
			}
		} else if (!coolerVal && humidity > MAX_HUMIDITY) {
			clearTimeout(delayed);
			coolerVal = true;
			startTime = Date.now();
			fanPowerCharacteristic.updateValue(true);
		}
	});
};
