const path = require('path');
const HAP = require('hap-nodejs');
const Bridge = HAP.Bridge;
const Accessory = HAP.Accessory;
const uuid = HAP.uuid;

module.exports = () => {
	console.log('HAP-NodeJS starting...');

	HAP.init(path.join(__dirname, 'persist'));

	const bridge = new Bridge('Home', uuid.generate('Home'));

	const accessories = []
			.concat(require('./accessories/light_accfactory'))
			.concat(require('./accessories/fan_accfactory'))
			.concat(require('./accessories/termo_accfactory'))
		;

	return Promise.all(accessories.map((a) => a.ready ? a.ready() : Promise.resolve()))
		.then(() => {
			console.log('Accessories ready');
			accessories.forEach((accessory) => bridge.addBridgedAccessory(accessory));

			bridge.publish({
				username: 'CC:22:3D:E3:CE:F6',
				port: 51826,
				pincode: '000-00-000',
				category: Accessory.Categories.BRIDGE
			});

			console.log('Accessories published');

			const automations = [
				require('./automation/bathroom'),
				require('./automation/light-triggers')
			];

			automations.forEach((a) => a(accessories));

			console.log('Automation was set up');
		})
		.catch((err) => {
			console.error(err);
		});
};

if (!module.parent) {
	module.exports();
}
