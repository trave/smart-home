const ledFactory = require('./lib/led-c-01/factory');
const singleton = require('./singleton');

module.exports = singleton(__filename, () => {
    const light = ledFactory({
        ip: '192.168.3.72',
        password: 'pass',
        channels: {
            '1': ['red', 'green', 'blue'],
            '2': ['red2', 'green2', 'blue2']
        },
        strips: {
            'bedroom': ['red'],
            'bathroom': ['green'],
            'guestroom1': ['blue'],
            'guestroom2': ['red2'],
            'guestroom34': ['green2'],
            'guestroom56': ['blue2']
        }
    });

    const backlight = ledFactory({
        ip: '192.168.3.73',
        password: 'pass',
        channels: {
            '1': ['red', 'green', 'blue'],
            '2': ['red2', 'green2', 'blue2']
        },
        strips: {
            'backlight-bathroom': ['red', 'green', 'blue'],
            'backlight-bedroom': ['red2', 'green2', 'blue2']
        }
    });

    return {
        getStripe: alias => light.getStripByAlias(alias) || backlight.getStripByAlias(alias),
        pull: () => Promise.all([light.pull(), backlight.pull()]),
        push: (callback) => {
            let promise = Promise.all([light.push(), backlight.push()]);
            if (callback) {
                promise = promise.then(() => callback(), callback);
            }

            return promise;
        },
        light,
        backlight,
        ready: Promise.all([
            light.pull(),
            backlight.pull()
        ])
    };
});