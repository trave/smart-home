#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root for access to GPIO" 1>&2
   exit 1
fi

export PATH=$PATH:/usr/local/bin:/opt/node/bin
export NODE_PATH=$NODE_PATH:/usr/local/lib/node_modules

SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"
FOREVER_DIR="$SCRIPT_DIR/forever"
NODE_MODULES_BIN="$SCRIPT_DIR/node_modules/.bin"
FOREVER_BIN="$NODE_MODULES_BIN/forever"
PID_FILE="$FOREVER_DIR/smart-home.pid"

case "$1" in
  start)
    if [ -f $PID_FILE ] && [ -n "$(ps -p `cat $PID_FILE` -o pid=)" ]; then
       echo "Daemon already running with PID `cat $PID_FILE`"
       exit 1
    fi

    $FOREVER_BIN start \
      -m 1 \
      --pidFile $PID_FILE \
      -l $FOREVER_DIR/smart-home.log \
      --append \
      --sourceDir $SCRIPT_DIR \
      index.js
  ;;
  stop)
    if [ -f $PID_FILE ] && [ -n "$(ps -p `cat $PID_FILE` -o pid=)" ]; then
       $FOREVER_BIN stop `cat $PID_FILE`
    fi
  ;;
  *)
    echo "Usage: /etc/init.d/smart-home {start|stop}"
    exit 1
  ;;
esac

exit 0
