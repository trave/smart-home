const Gpio = require('onoff').Gpio;
const HAP = require('hap-nodejs');
const Characteristic = HAP.Characteristic;
const Service = HAP.Service;
const Types = require('./../types.js');
const BasicAccessory = require('./basic-accessory');

class FanAccessory extends BasicAccessory {
    /**
     * @param {BasicAccessory.Input} options
     * @param {number} gpioPinNumber
     * @param {number} opt_powerOnValue
     */
    constructor(options, gpioPinNumber, opt_powerOnValue) {
        super(options);

        /**
         * @type {Gpio}
         * @protected
         */
        this._gpioPin = new Gpio(gpioPinNumber, 'out');
        /**
         * @type {Gpio}
         * @protected
         */
        this._powerOnValue = opt_powerOnValue || 0;

        this.addServices();
    }

    ready() {
	    return this._readValue()
		    .then((value) => {
			    this._characteristicPower.value = value === this._powerOnValue;
		    });
    }

    addServices() {
        super.addServices();

        this.addService(this._createFanService());
    }

    _sync() {
        return this._writeValue(this._characteristicPower.value ? this._powerOnValue : +!this._powerOnValue);
    }

    _createFanService() {
        const service = new Service('power', Types.FAN_STYPE, this._displayName);

        const characteristicPower = new Characteristic(Types.POWER_STATE_CTYPE, Types.POWER_STATE_CTYPE);
        characteristicPower.value = true;
        characteristicPower.setProps({
            format: Characteristic.Formats.BOOL,
            perms: [Characteristic.Perms.READ, Characteristic.Perms.WRITE, Characteristic.Perms.NOTIFY]
        });
        characteristicPower.on('change', () => this._sync());
        service.addCharacteristic(characteristicPower);

        this._characteristicPower = characteristicPower;

        return service;
    }

	_readValue() {
		return new Promise((resolve, reject) => {
			this._gpioPin.read((err, val) => {
				if (err) {
					reject(err);
				} else {
					resolve(val);
				}
			});
		});
	}

	_writeValue(val) {
		return new Promise((resolve, reject) => {
			this._gpioPin.write(val, (err) => {
				if (err) {
					reject(err);
				} else {
					resolve(val);
				}
			});
		});
	}
}

module.exports = FanAccessory;
