const sensorLib = require('node-dht-sensor');
const HAP = require('hap-nodejs');
const Characteristic = HAP.Characteristic;
const Service = HAP.Service;
const Types = require('./../types.js');
const BasicAccessory = require('./basic-accessory');

class TemperatureHumidityAccessory extends BasicAccessory {
    /**
     * @param {BasicAccessory.Input} options
     * @param {number} gpioPinNumber
     */
    constructor(options, gpioPinNumber) {
        super(options);

	    this._gpioPinNumber = gpioPinNumber;

	    this._readTimeoutId = NaN;

	    this.addServices();

	    this._startLoops();
    }

    ready() {
	    return Promise.resolve();
    }

    addServices() {
        super.addServices();

        this.addService(this._createTemperatureService());
        this.addService(this._createHumidityService());
    }

	_startLoops() {
		const median = (array, len, fractionalDigits, val) => {
			array.push(val);
			if (array.length >= len) {
				array.shift();
			}

			array.sort();

			const avgValue = array[Math.floor(array.length / 2)];

			return parseFloat(avgValue.toFixed(fractionalDigits));
		};
		const avgTemperature = median.bind(null, [], 5, 2);
		const avgHumidity = median.bind(null, [], 5, 2);
		const read = () => {
			const prevId = this._readTimeoutId;
			const next = () => {
				if (prevId === this._readTimeoutId) {
					this._readTimeoutId = setTimeout(read, 2000);
				}
			};
			this._readSensorData()
				.then((data) => {
					const temperature = avgTemperature(data.temperature);
					const humidity = avgHumidity(data.humidity);

					this._characteristicTemperature.updateValue(temperature);
					this._characteristicHumidity.updateValue(humidity);

					next();
				}, next);
		};
		this._readTimeoutId = -1;
		read();
	}

	_stopLoops() {
		clearTimeout(this._readTimeoutId);
		this._readTimeoutId = NaN;
	}

	_readSensorData() {
		return new Promise((resolve ,reject) => {
			sensorLib.read(22, this._gpioPinNumber, (err, temperature, humidity) => {
				if (err) {
					reject(err);
				} else {
					resolve({
						temperature,
						humidity
					});
				}
			});
		});
	}

    _createTemperatureService() {
        const service = new Service('temperature', Types.TEMPERATURE_SENSOR_STYPE, this._displayName);

        const characteristicTemperature = new Characteristic(Types.CURRENT_TEMPERATURE_CTYPE, Types.CURRENT_TEMPERATURE_CTYPE);
        characteristicTemperature.value = 0;
        characteristicTemperature.setProps({
            format: Characteristic.Formats.FLOAT,
	        unit: Characteristic.Units.CELSIUS,
	        maxValue: 100,
	        minValue: -65,
	        minStep: 0.1,
	        perms: [Characteristic.Perms.READ, Characteristic.Perms.NOTIFY]
        });
        service.addCharacteristic(characteristicTemperature);

        this._characteristicTemperature = characteristicTemperature;

        return service;
    }

    _createHumidityService() {
        const service = new Service('humidity', Types.HUMIDITY_SENSOR_STYPE, this._displayName);

        const characteristicHumidity = new Characteristic(Types.CURRENT_RELATIVE_HUMIDITY_CTYPE, Types.CURRENT_RELATIVE_HUMIDITY_CTYPE);
        characteristicHumidity.value = 0;
        characteristicHumidity.setProps({
            format: Characteristic.Formats.FLOAT,
	        unit: Characteristic.Units.PERCENTAGE,
	        maxValue: 100,
	        minValue: 0,
	        minStep: 0.01,
	        perms: [Characteristic.Perms.READ, Characteristic.Perms.NOTIFY]
        });
        service.addCharacteristic(characteristicHumidity);

        this._characteristicHumidity = characteristicHumidity;

        return service;
    }
}

module.exports = TemperatureHumidityAccessory;
