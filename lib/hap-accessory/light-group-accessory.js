const stripes = require('./../../stripes');
const LightAccessory = require('./light-accessory');

class LightGroupAccessory extends LightAccessory {
    /**
     * @param {BasicAccessory.Input} options
     * @param {Array<string>} stripeNames
     */
    constructor(options, stripeNames) {
        super(options, '');

        /**
         * @type {Array<Strip>}
         * @protected
         */
        this._strips = stripeNames.map((stripeName) => stripes.getStripe(stripeName));
    }

    ready() {
        return stripes.pull()
            .then(() => {
                const avgValue = Math.floor(this._strips.reduce((p, v) => p + v.value, 0) / this._strips.length);
                const percentageValue = Math.floor((avgValue / 255) * 100);
                this._characteristicBrightness.value = percentageValue;
                this._characteristicPower.value = avgValue !== 0;
            });
    }

    _setPowerState(value, callback) {
        if (value) {
            this._strips.forEach((s) => s.turnOn());
        } else {
            this._strips.forEach((s) => s.turnOff());
        }
        stripes.push(callback);
    }

    _setBrightnessValue(value, callback) {
        const calculatedValue = value / 100 * 255;
        this._strips.forEach((s) => s.value = calculatedValue);
        stripes.push(callback);
    }
}

module.exports = LightGroupAccessory;
