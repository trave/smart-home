const HAP = require('hap-nodejs');
const Characteristic = HAP.Characteristic;
const Types = require('./../types.js');
const stripes = require('./../../stripes');
const hsvRgbLib = require('./../hsv-rgb');
const LightAccessory = require('./light-accessory');


class BackLightAccessory extends LightAccessory {
    ready() {
        return stripes.pull()
            .then(() => {
                const [h, s, l] = hsvRgbLib.rgbToHsl(...this._strip.value);

                this._characteristicHue.value = h;
                this._characteristicSaturation.value = s;
                this._characteristicBrightness.value = l;

                this._characteristicPower.value = this._strip.value.some((v) => v !== 0);
            });
    }

    _createLightBulbService() {
        const service = super._createLightBulbService();

        const characteristicHue = new Characteristic(Types.HUE_CTYPE, Types.HUE_CTYPE);
        characteristicHue.value = 0;
        characteristicHue.setProps({
            format: Characteristic.Formats.INT,
            perms: [Characteristic.Perms.READ, Characteristic.Perms.WRITE, Characteristic.Perms.NOTIFY],
            minValue: 0,
            maxValue: 360,
            minStep: 1,
            unit: Characteristic.Units.ARC_DEGREE
        });
        characteristicHue.on('set', (value, callback) => {
            this._setHueValue(value, callback);
        });
        service.addCharacteristic(characteristicHue);

        this._characteristicHue = characteristicHue;


        const characteristicSaturation = new Characteristic(Types.SATURATION_CTYPE, Types.SATURATION_CTYPE);
        characteristicSaturation.value = 0;
        characteristicSaturation.setProps({
            format: Characteristic.Formats.INT,
            perms: [Characteristic.Perms.READ, Characteristic.Perms.WRITE, Characteristic.Perms.NOTIFY],
            minValue: 0,
            maxValue: 100,
            minStep: 1,
            unit: Characteristic.Units.PERCENTAGE
        });
        characteristicSaturation.on('set', (value, callback) => {
            this._setSaturationValue(value, callback);
        });
        service.addCharacteristic(characteristicSaturation);

        this._characteristicSaturation = characteristicSaturation;

        return service;
    }

    _setBrightnessValue(value, callback) {
        this._strip.value = hsvRgbLib.hsvToRgb(
            this._characteristicHue.value,
            this._characteristicSaturation.value,
            value
        );
        stripes.push(callback);
    }

    _setSaturationValue(value, callback) {
        this._strip.value = hsvRgbLib.hsvToRgb(
            this._characteristicHue.value,
            value,
            this._characteristicBrightness.value
        );
        stripes.push(callback);
    }

    _setHueValue(value, callback) {
        this._strip.value = hsvRgbLib.hsvToRgb(
            value,
            this._characteristicSaturation.value,
            this._characteristicBrightness.value
        );
        stripes.push(callback);
    }
}

module.exports = BackLightAccessory;
