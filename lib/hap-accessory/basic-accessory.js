const HAP = require('hap-nodejs');
const Accessory = HAP.Accessory;
const Characteristic = HAP.Characteristic;
const Service = HAP.Service;
const Types = require('./../types.js');

class BasicAccessory extends Accessory {
    /**
     * @param {BasicAccessory.Input} options
     */
    constructor(options) {
        super(options.name, HAP.uuid.generate(options.name));

        this._serialNumber = options.serialNumber;

        this.username = options.MAC;
        this.pincode = options.pin;

        this._displayName = options.name;
    }

    ready() {
        return Promise.resolve();
    }

    addServices() {
        this.services.length = 0;

        this.addService(this._createNameService());
    }

    _createNameService() {
        const service = new Service(this._displayName, Types.ACCESSORY_INFORMATION_STYPE, this._displayName);

        [
            {
                cType: Types.MANUFACTURER_CTYPE,
                perms: [Characteristic.Perms.READ],
                format: Characteristic.Formats.STRING,
                value: 'Oltica'
            },{
            cType: Types.MODEL_CTYPE,
            perms: [Characteristic.Perms.READ],
            format: Characteristic.Formats.STRING,
            value: 'Rev-1'
        },{
            cType: Types.SERIAL_NUMBER_CTYPE,
            perms: [Characteristic.Perms.READ],
            format: Characteristic.Formats.STRING,
            value: this._serialNumber
        },{
            cType: Types.IDENTIFY_CTYPE,
            perms: [Characteristic.Perms.WRITE],
            format: Characteristic.Formats.BOOL,
            value: false
        }
        ].forEach((data) => {
            const characteristic = new Characteristic(data.cType, data.cType);
            characteristic.value = data.value;
            characteristic.setProps({
                format: data.format,
                perms: data.perms
            });

            service.addCharacteristic(characteristic);
        });

        return service;
    }

}


/**
 * @typedef {{
 *      MAC: string,
 *      name: string,
 *      pin: string,
 *      serialNumber: string
 * }}
 */
BasicAccessory.Input;

module.exports = BasicAccessory;
