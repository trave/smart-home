const HAP = require('hap-nodejs');
const Characteristic = HAP.Characteristic;
const Service = HAP.Service;
const Types = require('./../types.js');
const stripes = require('./../../stripes');
const BasicAccessory = require('./basic-accessory');

class LightAccessory extends BasicAccessory {
    /**
     * @param {BasicAccessory.Input} options
     * @param {string} stripeName
     */
    constructor(options, stripeName) {
        super(options);

        /**
         * @type {Strip}
         * @protected
         */
        this._strip = stripes.getStripe(stripeName);

        this.addServices();
    }

    ready() {
        return stripes.pull()
            .then(() => {
                this._characteristicBrightness.value = this._strip.value;
                this._characteristicPower.value = this._strip.value !== 0;
            });
    }

    addServices() {
        super.addServices();

        this.addService(this._createLightBulbService());
    }

    _createLightBulbService() {
        const service = new Service('power', Types.LIGHTBULB_STYPE, this._displayName);

        const characteristicPower = new Characteristic(Types.POWER_STATE_CTYPE, Types.POWER_STATE_CTYPE);
        characteristicPower.value = true;
        characteristicPower.setProps({
            format: Characteristic.Formats.BOOL,
            perms: [Characteristic.Perms.READ, Characteristic.Perms.WRITE, Characteristic.Perms.NOTIFY]
        });
        characteristicPower.on('set', (value, callback) => {
            this._setPowerState(value, callback);
        });
        service.addCharacteristic(characteristicPower);

        this._characteristicPower = characteristicPower;

        const characteristicBrightness = new Characteristic(Types.BRIGHTNESS_CTYPE, Types.BRIGHTNESS_CTYPE);
        characteristicBrightness.value = 0;
        characteristicBrightness.setProps({
            format: Characteristic.Formats.INT,
            perms: [Characteristic.Perms.READ, Characteristic.Perms.WRITE, Characteristic.Perms.NOTIFY],
            minValue: 0,
            maxValue: 100,
            minStep: 1,
            unit: Characteristic.Units.PERCENTAGE
        });
        characteristicBrightness.on('set', (value, callback) => {
            this._setBrightnessValue(value, callback);
        });
        service.addCharacteristic(characteristicBrightness);

        this._characteristicBrightness = characteristicBrightness;

        return service;
    }

    _setPowerState(value, callback) {
        if (value) {
            this._strip.turnOn();
        } else {
            this._strip.turnOff();
        }
        stripes.push(callback);
    }

    _setBrightnessValue(value, callback) {
        this._strip.value = value / 100 * 255;
        stripes.push(callback);
    }
}

module.exports = LightAccessory;
