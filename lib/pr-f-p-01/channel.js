class Channel {
	constructor() {
		this._state = Channel.State.WAIT;
		this._min = -Infinity;
		this._max = Infinity;
		this._alias = '';
		this._isOn = false;
		this.resetTerm();
	}
	/**
	 * Set default term value
	 */
	resetTerm() {
		this._term = -273.15;
	}
	/**
	 * @param {Channel.State} state
	 */
	set state(state) {
		this._state = state;
	}
	/**
	 * @return {Channel.State}
	 */
	get state() {
		return this._state;
	}
	/**
	 * @param {number} term
	 */
	set term(term) {
		this._term = term;
	}
	/**
	 * @return {number}
	 */
	get term() {
		return this._term;
	}
	/**
	 * @param {number} index
	 */
	set index(index) {
		return this._index = index;
	}
	/**
	 * @return {number}
	 */
	get index() {
		return this._index;
	}
	/**
	 * @param {string} alias
	 */
	set alias(alias) {
		this._alias = alias;
	}
	/**
	 * @return {string}
	 */
	get alias() {
		return this._alias;
	}
	/**
	 * @param {number} max
	 */
	set max(max) {
		this._max = max;
	}
	/**
	 * @return {number}
	 */
	get max() {
		return this._max;
	}
	/**
	 * @param {number} min
	 */
	set min(min) {
		this._min = min;
	}
	/**
	 * @return {number}
	 */
	get min() {
		return this._min;
	}
	/**
	 * @param {boolean} isOn
	 */
	set isOn(isOn) {
		this._isOn = isOn;
	}
	/**
	 * @return {boolean}
	 */
	get isOn() {
		return this._isOn;
	}
	/**
	 * @override
	 */
	toString() {
		return `${this.index} AKA "${this.alias}" is ${this.state}. Term: ${this.term} Min: ${this.min} Max: ${this.max}`;
	}
}

/**
 * @enum {string}
 */
Channel.State = {
	WAIT: 'wait',
	ERROR: 'error',
	OK: 'ok'
};

module.exports = Channel;
