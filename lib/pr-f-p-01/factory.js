const WarmFloorController = require('./controller');
const Channel = require('./channel');

/**
 * ip - Controller address.
 * password - Controller password.
 * channels - Object with alias as key and index as value.
 * @param {{
	 *      ip: string,
	 *      password: string,
	 *      channels: Object<string,number>
	 * }} data
 * @return {WarmFloorController}
 */
module.exports = data => {
	let controller = new WarmFloorController(data.ip, data.password);
	controller.channels = Object
		.keys(data.channels)
		.map(alias => {
			let channel = new Channel();
			channel.alias = alias;
			channel.index = data.channels[alias];
			return channel;
		});

	return controller;
};
