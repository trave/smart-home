const floorFactory = require('./factory');

let controller = floorFactory({
	ip: '192.168.2.15',
	password: 'sec',
	channels: {
		'bathroom': 1,
		'kitchen': 2,
		'window in badroom': 3,
		'window in guest room': 4
	}
});

let debug = (data) => {
	console.log(controller.channels.map(String));
	return data;
};

debug();

controller.channels[0].isOn = !controller.channels[0].isOn;

controller.pull()
	.then(() => {
		return controller.push();
	})
	.then(() => {
		debug();
		console.log('DONE');
	}, e => {
		console.log('ERR', e);
	});

