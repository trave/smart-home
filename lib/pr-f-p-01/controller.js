const http = require('http');
const Channel = require('./channel');

class WarmFloorController {
	/**
	 * @param {string} ip
	 * @param {string} password
	 */
	constructor(ip, password) {
		/** @type {string} */
		this._ip = ip;
		/** @type {string} */
		this._password = password;
		/** @type {Array<Channel>} */
		this._channels = [];
	}

	/**
	 * @return {Array<Channel>}
	 */
	get channels() {
		return this._channels;
	}

	/**
	 * @param {Array<Channel>} channels
	 */
	set channels(channels) {
		this._channels = channels;
	}

	/**
	 * @return {Promise}
	 */
	push() {
		var data = {
			'frm': 1
		};

		this._channels.forEach(channel => {
			data['W' + channel.index] = channel.isOn ? 1 : 0;
			data['t1' + channel.index + 'min'] = channel.min;
			data['t' + channel.index + 'max'] = channel.max;
		});

		return this._request(data);
	}

	/**
	 * @return {Promise}
	 */
	pullTerm() {
		return Promise.all(this.channels.map(ch => this._pullChannelData(ch).then(this._updateChannel.bind(this, ch))));
	}

	/**
	 * @return {Promise}
	 */
	pull() {
		return this._request({'frm': 1})
			.then(response => {
				// parse term info
				let exp = /name=t(\d+)(min|max) value=(\d+)/g;
				let result;
				while(result = exp.exec(response)) {
					let [index, type, value] = result.slice(1).slice(0, 3);
					index = parseInt(index, 10);
					value = parseFloat(value);
					let channel = this._channels.find(ch => ch.index === index);
					if (channel) {
						if (type === 'min') {
							channel.min = value;
						} else if (type === 'max') {
							channel.max = value;
						}
					}
				}

				// parse on/off state
				let isOnExp = /<select name=W(\d+)>[^\/]+(selected value=(0|1))/g;
				let isOnResult;
				while(isOnResult = isOnExp.exec(response)) {
					let [, index, , isOn] = isOnResult;
					isOn = parseInt(isOn, 10);
					let channel = this._channels.find(ch => ch.index === index);
					if (channel) {
						channel.isOn = isOn === 1;
					}
				}
			})
			.then(this.pullTerm.bind(this));
	}

	_pullChannelData(channel) {
		return this._request({'term': channel.index});
	}

	_updateChannel(channel, data) {
		switch (data) {
			case 'error':
				channel.state = Channel.State.ERROR;
				channel.resetTerm();
				break;
			case 'wait':
				channel.state = Channel.State.WAIT;
				break;
			default:
				channel.state = Channel.State.OK;
				channel.term = parseFloat(data);
				break;
		}
		return channel;
	}


	/**
	 * @param {Object} data
	 * @return {Promise<string>}
	 * @protected
	 */
	_request(data) {
		let params = Object.keys(data)
			.map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
			.join('&');

		let url = 'http://' + this._ip + '/' + this._password + '/?' + params;

		return new Promise((resolve, reject) => {
			http.get(url)
				.on('response', response => {
					response = /** @type {http.IncomingMessage} */(response);

					let body = '';
					response.on('data', chunk => body += chunk.toString());
					response.on('end', () => resolve(body));
				})
				.on('error', e => {
					console.log(`Got HTTP error: ${e.message}`);
					reject(e);
				});
		});
	}
}

module.exports = WarmFloorController;
