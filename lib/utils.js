const HAP = require('hap-nodejs');
const Characteristic = HAP.Characteristic;
const Accessory = HAP.Accessory;


/**
 * @param {Array<Accessory>} accessories
 * @param {string} mac
 * @param {string} serviceName
 * @param {string} characteristicCType
 * @return {?Characteristic}
 */
const findCharacteristic = (accessories, mac, serviceName, characteristicCType) => {
	const accessory = accessories.find((a) => a.username === mac);
	if (accessory) {
		const service = accessory.getService(serviceName);
		if (service) {
			const characteristic = service.getCharacteristic(characteristicCType);
			if (!characteristic) {
				console.error('cant find characteristic', characteristicCType);
			}
			return characteristic;
		} else {
			console.error('cant find service', serviceName);
		}
	} else {
		console.error('cant find accessory', mac);
	}

	return null;
};

module.exports = {
	findCharacteristic: findCharacteristic
};