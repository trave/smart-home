const Channel = require('./channel');

class ChannelGroup {
	/**
	 * @param {string} name
	 */
	constructor(name) {
		/** @type {string} */
		this._name = name;
		/** @type {Array<Channel>} */
		this._channels = [];
	}

	/**
	 * @param {Array<Channel>} channels
	 */
	set channels(channels) {
		this._channels = channels;
	}

	/**
	 * @return {Array<Channel>}
	 */
	get channels() {
		return this._channels;
	}

	/**
	 * @return {string}
	 */
	get alias() {
		return this._name;
	}

	/**
	 * @param {string} alias
	 */
	set alias(alias) {
		this._name = alias;
	}

	get paramsPostfix() {
		return this.alias === '1' ? '' : this.alias;
	}

	toString() {
		return this._name;
	}
}

module.exports = ChannelGroup;
