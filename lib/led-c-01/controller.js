const http = require('http');
const EventEmitter = require('events');
const Channel = require('./channel');
const Group = require('./group');
const AbstractStrip = require('./abstract-strip');

/**
 * LED-C-01 controller wrapper
 * See http://it-home.pro/ for device details.
 */
class LedController extends EventEmitter {
	/**
	 * @param {string} ip
	 * @param {string} password
	 */
	constructor(ip, password) {
		super();

		/** @type {string} */
		this._ip = ip;
		/** @type {string} */
		this._password = password;
		/** @type {Array<Channel>} */
		this._channels = [];
		/** @type {Array<AbstractStrip>} */
		this._strips = [];
		/** @type {Group} */
		this._groups = [];
	}

	/**
	 * @param {Array<Group>} groups
	 */
	set groups(groups) {
		this._groups = groups;
		this._channels = [];
		this._groups
			.forEach(group => {
				this._channels = this._channels.concat(group.channels);
			});
	}

	/**
	 * @return {Array<Group>}
	 */
	get groups() {
		return this._groups;
	}

	/**
	 * @param {Array<AbstractStrip>} strips
	 */
	set strips(strips) {
		this._strips = strips;
	}

	/**
	 * @return {Array<AbstractStrip>}
	 */
	get strips() {
		return this._strips;
	}

	getStripByAlias(alias) {
		return this._strips.find(s => s.alias === alias);
	}

	/**
	 * @return {Promise<boolean>}
	 */
	isAvailable() {
		return this._request({'frm': 0})
			.then(response => response.indexOf('LED controller') !== -1);
	}

	/**
	 * @param {Channel} channel
	 * @return {boolean}
	 */
	isChannelFree(channel) {
		let usedChannels = [];
		this._strips.forEach(strip => {
			usedChannels = usedChannels.concat(strip.getChannels());
		});
		return usedChannels.indexOf(channel) === -1;
	}

	/**
	 * @param {Group=} group
	 * @param {number=} duration
	 * @param {number=} timeout
	 * @return {Promise}
	 */
	push({group, duration = 0, timeout = duration * 50} = {}) {
		let groups = group ? [group] : this.groups;
		let requestData = [];

		return Promise
			.all(groups.map(group => {
				let data = {
					'frm': group.alias,
					['smo' + group.paramsPostfix]: duration
				};

				group.channels
					.forEach(ch => {
						const val = ch.isOn ? ch.value : 0;
						data[ch.alias] = val;

						requestData[ch.alias] = val;
					});

				return this._request(data);
			}))
			.then(() => {
				if (duration) {
					return this.waitFor(requestData, timeout);
				}
			});
	}

	/**
	 * @return {Promise}
	 */
	pull() {
		return Promise.all(this.groups.map(g => this._request({'frm': g.alias})))
			.then((responses) => {
				responses.forEach((response) => {
					let exp = /(\w+) \((\d+)\)/g;
					let parsed;
					while (parsed = exp.exec(response)) {
						let [all, alias, value] = parsed;
						alias = alias.toLowerCase();
						value = parseInt(value, 10);
						let channel = this.getChannelByAlias(alias);
						if (!channel) {
							throw new Error('Can\'t find channel with alias ' + alias);
						}
						channel.value = value;
					}
				});
			});
	}

	/**
	 * @param {Object} data Key is channel alias and value is channel value
	 * @param {number} timeout
	 * @return {Promise}
	 */
	waitFor(data, timeout) {
		return new Promise((resolve, reject) => {
			const start = Date.now();

			let loop = () => {
				if (Object.keys(data).length === 0) {
					resolve();
				} else if (Date.now() - start > timeout) {
					reject();
				} else {
					this.pull()
						.then(() => {
							Object.keys(data)
								.forEach(alias => {
									let ch = this.getChannelByAlias(alias);
									if (ch.value === data[alias]) {
										delete data[alias];
									}
								});
							setTimeout(loop, 50);
						});
				}
			};
			loop();
		});
	}

	/**
	 * @return {Array<Channel>}
	 */
	getChannels() {
		return this._channels.slice(0);
	}

	/**
	 * @param {string} alias
	 * @return {?Channel}
	 */
	getChannelByAlias(alias) {
		return this._channels.find(ch => ch.alias === alias) || null;
	}


	/**
	 * @param {Object} data
	 * @return {Promise<string>}
	 * @protected
	 */
	_request(data) {
		let params = Object.keys(data)
			.map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
			.join('&');

		let url = 'http://' + this._ip + '/' + this._password + '/?' + params;

		return new Promise((resolve, reject) => {
			http.get(url)
				.on('response', response => {
					response = /** @type {http.IncomingMessage} */(response);

					let body = '';
					response.on('data', chunk => body += chunk.toString());
					response.on('end', () => resolve(body));
				})
				.on('error', e => {
					console.log(`Got HTTP error: ${e.message}`);
					reject(e);
				});
		});
	}
}

module.exports = LedController;
