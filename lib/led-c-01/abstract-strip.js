const Channel = require('./channel');

class IStrip {
	constructor() {
		/**
		 * @type {string}
		 * @protected
		 */
		this._alias = '';

		/**
		 * @type {boolean}
		 * @protected
		 */
		this._isOn = false;
	}

	/**
	 * Turn LED strip off.
	 */
	turnOff() {
		this._isOn = false;
		this.getChannels().forEach((c) => c.isOn = false);
	}

	/**
	 * Turn LED strip off.
	 */
	turnOn() {
		this._isOn = true;
		this.getChannels().forEach((c) => c.isOn = true);
	}

	isOn() {
		return this._isOn;
	}

	/**
	 * @return {Array<Channel>}
	 */
	getChannels() {}

	/**
	 * @param {string} alias
	 */
	set alias(alias) {
		this._alias = alias;
	}

	/**
	 * @return {string}
	 */
	get alias() {
		return this._alias;
	}
}

module.exports = IStrip;
