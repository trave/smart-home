const Channel = require('./channel');
const AbstractStrip = require('./abstract-strip');

class Strip extends AbstractStrip {
	/**
	 * @param {Channel} channel
	 */
	constructor(channel) {
		super();

		/**
		 * @type {Channel}
		 * @protected
		 */
		this._channel = channel;
	}

	/**
	 * @override
	 */
	getChannels() {
		return [this._channel];
	}

	/**
	 * @param {number} value
	 */
	set value(value) {
		this._channel.value = value;
	}

	/**
	 * @return {number}
	 */
	get value() {
		return this._channel.value;
	}
}

module.exports = Strip;
