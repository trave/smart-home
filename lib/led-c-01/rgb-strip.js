const Channel = require('./channel');
const AbstractStrip = require('./abstract-strip');

class RGBStrip extends AbstractStrip {
	/**
	 * @param {RGBStrip.Config} channels
	 */
	constructor(channels) {
		super();

		/**
		 * @type {RGBStrip.Config}
		 * @protected
		 */
		this._config = channels;
	}

	/**
	 * @override
	 */
	getChannels() {
		return [this._config.red, this._config.green, this._config.blue];
	}

	/**
	 * @param {Array<number>} array
	 */
	set value(array) {
		[
			this._config.red.value,
			this._config.green.value,
			this._config.blue.value
		] = array;
	}

	/**
	 * @return {Array<number>}
	 */
	get value() {
		return [
			this._config.red.value,
			this._config.green.value,
			this._config.blue.value
		];
	}
}

/**
 * @typedef {{
 *      red: Channel,
 *      green: Channel,
 *      blue: Channel
 * }}
 */
RGBStrip.Config;

module.exports = RGBStrip;
