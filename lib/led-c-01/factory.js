const Controller = require('./controller');
const Channel = require('./channel');
const Group = require('./group');
const RGBStrip = require('./rgb-strip');
const Strip = require('./strip');

/**
 * ip - Controller address.
 * password - Controller password.
 * channels - Object with group name as key and array of channel aliases as value.
 * strips - Object with strip alias as key and channels array as value. For strips with one channel will be create Strip object, for strips with 3 channels will be created RGBStrip object.
 * @param {{
	 *      ip: string,
	 *      password: string,
	 *      channels: Object<string,Array<string>>,
	 *      strips: Object<string,Array<string>>
	 * }} data
 * @return {LedController}
 */
module.exports = data => {
	let controller = new Controller(data.ip, data.password);
	controller.groups = Object
		.keys(data.channels)
		.map(groupAlias => {
			let group = new Group(groupAlias);
			group.channels = data
				.channels[groupAlias]
				.map(chName => new Channel(chName));
			return group;
		});

	controller.strips = Object
		.keys(data.strips)
		.map(alias => {
			let channels = data.strips[alias].map(alias => controller.getChannelByAlias(alias));
			/** @type {AbstractStrip} */
			let strip;
			if (channels.length === 3) {
				strip = new RGBStrip({
					red: channels[0],
					green: channels[1],
					blue: channels[2]
				});
			} else {
				strip = new Strip(channels[0]);
			}

			strip.alias = alias;

			return strip;
		});

	return controller;
};
