class Channel {
	/**
	 * @param {string} alias
	 * @param {number=} opt_value
	 */
	constructor(alias, opt_value) {
		/** @type {string} */
		this._alias = alias;
		/** @type {number} */
		this._value = opt_value || 0;
		/** @type {boolean} */
		this._isOn = this._value !== 0;
	}

	/**
	 * @return {number}
	 */
	get value() {
		return this._value;
	}

	/**
	 * @param {number} val
	 */
	set value(val) {
		this._value = val;
	}

	/**
	 * @return {boolean}
	 */
	get isOn() {
		return this._isOn;
	}

	/**
	 * @param {boolean} val
	 */
	set isOn(val) {
		this._isOn = val;
	}

	/**
	 * @return {string}
	 */
	get alias() {
		return this._alias;
	}

	/**
	 * @return {string}
	 */
	toString() {
		return 'Channel ' + this._alias + ' is ' + this._value;
	}
}


module.exports = Channel;
