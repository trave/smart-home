const ledFactory = require('./factory');

let controller = ledFactory({
	ip: '192.168.2.45',
	password: 'pass',
	channels: {
		'1': ['red', 'green', 'blue'],
		'2': ['red2', 'green2', 'blue2']
	},
	strips: {
		'bathroom': ['red', 'green', 'blue'],
		'badroom': ['red2', 'green2', 'blue2']
	}
});

let debug = (data) => {
	console.log(controller.getChannels().map(String));
	return data;
};

debug();

controller.getStripByAlias('badroom').value = [0, 0, 0];
controller.getStripByAlias('bathroom').value = [0, 0, 0];

controller.push({duration: 500})
	.then(() => {
		debug();
		console.log('DONE');
	}, e => {
		console.log('ERR', e);
	});
