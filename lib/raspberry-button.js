const EventEmitter = require('events');
const Gpio = require('onoff').Gpio;

/**
 * RaspberryPi GPIO button wrapper
 */
class RaspberryButton extends EventEmitter {
	/**
	 * @param {number} gpioNumber
	 */
	constructor(gpioNumber) {
		super();

		/** @type {Gpio} */
		this._gpio = new Gpio(gpioNumber, 'in', 'both');

		/** @type {boolean} */
		this._pressed = false;

		/** @type {number} */
		this._pressStart = 0;

		this._gpio.watch(this._onGpioEvent.bind(this));

		/** @const {string} Fired with: number - duration */
		this.EVENT_PRESS = 'press';
		/** @const {string} Fired without arguments */
		this.EVENT_DOWN = 'down';
		/** @const {string} Fired with: number - duration */
		this.EVENT_UP = 'up';
	}

	/**
	 * @return {number}
	 */
	get gpioNumber() {
		return this._gpio.gpio;
	}

	destroy() {
		this._gpio.unexport();
	}

	_onGpioEvent(err, value) {
		if (err) {
			throw err;
		}

		var isPressed = value == 0;

		if (isPressed !== this._pressed) {
			this._pressed = isPressed;
			this._onButtonChangeState(this._pressed);
		}
	}

	_onButtonChangeState(pressed) {
		if (pressed) {
			this.emit(this.EVENT_DOWN);
			this._pressStart = Date.now();
		} else {
			var pressDuration = (Date.now() - this._pressStart) / 1000;
			this.emit(this.EVENT_UP, pressDuration);
			this.emit(this.EVENT_PRESS, pressDuration);
		}
	}
}

module.exports = RaspberryButton;
