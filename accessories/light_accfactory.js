const Backlight = require('./../lib/hap-accessory/backlight-accessory');
const Light = require('./../lib/hap-accessory/light-accessory');
const LightGroup = require('./../lib/hap-accessory/light-group-accessory');

module.exports = [
    new Light({
        MAC: '1A:2B:3C:4D:5E:EC',
        name: 'Bathroom light',
        pin: '000-00-000',
        serialNumber: 'A1S2NASF88EW'
    }, 'bathroom'),
    new Light({
        MAC: '1A:2B:3C:4D:5E:ED',
        name: 'Bedroom light',
        pin: '000-00-000',
        serialNumber: 'A1S2NASF88EW'
    }, 'bedroom'),
    new LightGroup({
        MAC: '1A:2B:3C:4D:5E:1D',
        name: 'Living Room light',
        pin: '000-00-000',
        serialNumber: 'A1S2NASF88EW'
    }, [
        'guestroom1',
        'guestroom2',
        'guestroom34',
        'guestroom56'
    ]),
    new Backlight({
        MAC: '1A:2B:3C:4D:5E:EF',
        name: 'Bedroom back light',
        pin: '000-00-000',
        serialNumber: 'A1S2NASF88EW'
    }, 'backlight-bedroom'),
    new Backlight({
        MAC: '1A:2B:3C:4D:5E:AF',
        name: 'Bathroom back light',
        pin: '000-00-000',
        serialNumber: 'A1S2NASF88EW'
    }, 'backlight-bathroom')
];
