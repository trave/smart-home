const Fan = require('./../lib/hap-accessory/fan-accessory');

module.exports = [
    new Fan({
        MAC: '1A:2B:3C:4D:01:EC',
        name: 'Fan Bathroom',
        pin: '000-00-000',
        serialNumber: 'A1S2NASF88EW'
    }, 3, 1),
    new Fan({
        MAC: '1A:2B:3C:4D:02:EC',
        name: 'Fan server',
        pin: '000-00-000',
        serialNumber: 'A1S2NASF88EW'
    }, 4, 1)
];
