const TemperatureHumidity = require('./../lib/hap-accessory/temperature-humidity-accessory');

module.exports = [
    new TemperatureHumidity({
        MAC: '1A:2B:3C:4D:11:EC',
        name: 'Termo Bathroom',
        pin: '000-00-000',
        serialNumber: 'A1S2NASF88EW'
    }, 10),
    new TemperatureHumidity({
        MAC: '1A:2B:3C:4D:12:EC',
        name: 'Termo server',
        pin: '000-00-000',
        serialNumber: 'A1S2NASF88EW'
    }, 9)
];
